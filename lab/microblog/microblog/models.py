# -*- coding: utf-8 -*-

from persistent.mapping import PersistentMapping
from persistent import Persistent


class Blog(PersistentMapping):
    """ Klasa dla kontenera """
    # Należy ją odpowiednio zainicjalizować
    __name__ = None
    __parent__ = None
    posts=[]
    def addPost(self,post):
        self.posts.append(post)

class Post(Persistent):
    """ Klasa dla pojedynczego wpisu """
    # Należy dorobić odpowiedni konstruktor
    def __init__(self, title, content):
        self.title=title
        self.content=content
    def __unicode__(self):
        return self.title


def appmaker(zodb_root):
    if not 'app_root' in zodb_root:
        # Utworzenie głównegp kontenera
        app_root = Blog()
        post=Post("asd","zxc")
        app_root['first']=post
        post.__name__='First'
        post.__parent__=app_root
        zodb_root['app_root'] = app_root
        import transaction
        transaction.commit()
    return zodb_root['app_root']
