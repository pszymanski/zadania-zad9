# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from docutils.core import publish_parts
from .models import Blog, Post

# Główna strona
@view_config(context='.models.Blog',renderer='templates/list.pt')
def view_blog(context, request):
    return dict(context=context)

@view_config(context='.models.Post', renderer='templates/view.pt')
def view_post(context, request):
    wiki = context.__parent__

    def check(match):
        word = match.group(1)
        if word in wiki:  # Jest strona dla danego WikiSłowa
            page = wiki[word]
            view_url = request.resource_url(page)
            return u'<a href="%s">%s</a>' % (view_url, word)
        else:  # Dana strona jeszcze nie istnieje
            add_url = request.resource_url(wiki, 'add_page', word)
            return u'<a href="%s">%s</a>' % (add_url, word)

    # Przekształcenie tekstu z formatu ReST na HTML
    content = publish_parts(context.content, writer_name='html')['html_body']
    # Podmiana WikiSłów na odnośniki do stron
    return dict(post=context)


@view_config(name='add', context='.models.Blog')
def add_post(context, request):
    # Pierwszy element ścieżki po bieżącym kontekście i widoku
    if request.method == 'POST':
        postname=request.params['title']
        post = Post(title=request.params['title'], content=request.params['content'])
        post.__name__ = request.params['title']
        post.__parent__ = context
        # Dodanie nowej strony do kolekcji stron w klasie Wiki
        context[postname] = post
        return HTTPFound(location=request.resource_url(post))
