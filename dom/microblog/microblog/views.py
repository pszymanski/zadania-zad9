# -*- coding: utf-8 -*-

from pyramid.httpexceptions import HTTPFound
from pyramid.view import view_config
from docutils.core import publish_parts
from .models import Blog, Post

# Główna strona
@view_config(context='.models.Blog',renderer='templates/list.pt')
def view_blog(context, request):
    return dict(context=context)

@view_config(context='.models.Post', renderer='templates/view.pt')
def view_post(context, request):
    blog = context.__parent__

    def check(match):
        word = match.group(1)
        if word in blog:
            page = blog[word]
            view_url = request.resource_url(page)
            return u'<a href="%s">%s</a>' % (view_url, word)
        else:  # Dana strona jeszcze nie istnieje
            add_url = request.resource_url(blog, 'add_page', word)
            return u'<a href="%s">%s</a>' % (add_url, word)

    content = publish_parts(context.content, writer_name='html')['html_body']
    return dict(post=context)


@view_config(name='add', context='.models.Blog')
def add_post(context, request):
    if request.method == 'POST':
        if 'parent' in request.params:
            post=context[request.params['parent']].addcomment(request.params['content'])
            context[post.__name__]=post
            return HTTPFound(location=request.resource_url(context))
        else:
            postname=request.params['title']
            post = Post(title=request.params['title'], content=request.params['content'])
            post.__name__ = request.params['title']
            post.__parent__ = context
            context.addPost(post)
            return HTTPFound(location=request.resource_url(context))
    return dict(post=context)
