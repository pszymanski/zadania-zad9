# -*- coding: utf-8 -*-

from persistent.mapping import PersistentMapping
from persistent import Persistent


class Blog(PersistentMapping):
    """ Klasa dla kontenera """
    # Należy ją odpowiednio zainicjalizować
    __name__ = None
    __parent__ = None
    def addPost(self,post):
        self[post.title]=post
    def getPosts(self):
        postsOnly=[]
        for post in dict(self).values():
            if post.__name__[:7] != "comment":
                postsOnly.append(post)
        return postsOnly

class Post(Persistent):
    """ Klasa dla pojedynczego wpisu """
    # Należy dorobić odpowiedni konstruktor
    def __init__(self, title, content):
        self.title=title
        self.content=content
        self.comments={}
    def addcomment(self,message):
        comment=Post(title="comment_"+self.title+"_"+str(len(self.comments)), content=message)
        comment.__name__=comment.title
        comment.__parent__=self
        self.comments[comment.title]=comment
        self._p_changed=True
        return comment
    def countcomments(self):
        i=0
        for comment in self.comments.values():
            i=i+comment.countcomments()
            i=i+1
        return i
    def __unicode__(self):
        return self.title

def appmaker(zodb_root):
    if not 'app_root' in zodb_root:
        # Utworzenie głównegp kontenera
        app_root = Blog()
        zodb_root['app_root'] = app_root
        import transaction
        transaction.commit()
    return zodb_root['app_root']
