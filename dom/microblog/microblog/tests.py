# -*- coding: utf-8 -*-

import unittest

from pyramid import testing


####################
#   Testy modeli   #
####################

class BlogModelTests(unittest.TestCase):

    def _getTargetClass(self):
        from .models import Blog
        return Blog

    def _makeOne(self):
        return self._getTargetClass()()

    def test_it(self):
        blog = self._makeOne()
        self.assertEqual(blog.__parent__, None)
        self.assertEqual(blog.__name__, None)


class PostModelTests(unittest.TestCase):

    def _getTargetClass(self):
        from .models import Post
        return Post

    def _makeOne(self, title=u'some title', content=u'some content'):
        return self._getTargetClass()(title=title, content=content)

    def test_constructor(self):
        instance = self._makeOne()
        self.assertEqual(instance.title, u'some title')
        self.assertEqual(instance.content, u'some content')


class AppmakerTests(unittest.TestCase):

    def _callFUT(self, zodb_root):
        from .models import appmaker
        return appmaker(zodb_root)

    def test_it(self):
        root = {}
        self._callFUT(root)
        self.assertEqual(root['app_root']['first'].title,
                         'asd')
        self.assertEqual(root['app_root']['first'].content,
                         'zxc')



#####################
#   Testy widoków   #
#####################


class ViewBlogTests(unittest.TestCase):
    def test_it(self):
        from .views import view_blog
        context = testing.DummyResource()
        request = testing.DummyRequest()
        response = view_blog(context, request)
        self.assertEqual(response['context'], context)


class ViewPostTests(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import view_post
        return view_post(context, request)

    def test_it(self):
        blog = testing.DummyResource()
        blog[u'NaprawdęIstnieje'] = testing.DummyResource()
        context = testing.DummyResource(title=u'Witaj', content=u'asd')
        context.__parent__ = blog
        context.__name__ = 'post'
        request = testing.DummyRequest()
        info = self._callFUT(context, request)
        self.assertEqual(info['post'], context)


class AddPostTests(unittest.TestCase):
    def _callFUT(self, context, request):
        from .views import add_post
        return add_post(context, request)

    def test_it_notsubmitted(self):
        context = testing.DummyResource()
        request = testing.DummyRequest()
        request.subpath = ['InnyPost']
        info = self._callFUT(context, request)
        self.assertEqual(info['post'] ,context)

    def test_it_submitted(self):
        context = testing.DummyResource(title="asd", content="zxc")
        request = testing.DummyRequest()
        request.subpath = ['AnotherPost']
        self._callFUT(context, request)
        post = context
        self.assertEqual(post.title, 'asd')
        self.assertEqual(post.__name__, None)
        self.assertEqual(post.__parent__, None)

