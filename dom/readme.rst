
Praca domowa #9
===============

Dokończyć aplikację z zajęć oraz rozbudować ją o następujące elementy:

* formatowanie teskstu za pomocą ReStrukturedText
* możliwość komentowania wpisów (komentarze do pojedynczego wpisu oraz komentarze do komentarzy)
  wraz z wyświetlaniem całkowitej liczby komentarzy dla danego wpisu na stronie głównej oraz
  pełnego drzewka komentarzy na stronie z widokiem szczegółowym

Dla każdego modelu, widoku, funkjonalności i możliwego przypadku należy stworzyć odowiednie testy.
Proszę też dorobić schludny i oryginalny wygląd dla aplikacji.